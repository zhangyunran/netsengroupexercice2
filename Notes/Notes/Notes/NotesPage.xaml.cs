﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Xamarin.Forms;
using Notes.Models;

namespace Notes
{
    public partial class NotesPage : ContentPage
    {
        public NotesPage()
        {
            InitializeComponent();
        }

        //Populates the ListView with any notes stored in the database
        protected override async void OnAppearing()
        {
            base.OnAppearing();

            listView.ItemsSource = await App.Database.GetNotesAsync();
        }

        //When the ToolbarItem is pressed the OnNoteAddedClicked event handler is executed
        async void OnNoteAddedClicked(object sender, EventArgs e)
        {
            //Navigates to the NoteEntryPage, setting the BindingContext of the NoteEntryPage to a new Note instance
            await Navigation.PushAsync(new NoteEntryPage
            {
                BindingContext = new Note()
            });
        }

        //When an item in the ListView is selected the OnListViewItemSelected event handler is executed
        async void OnListViewItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            //Navigates to the NoteEntryPage, setting the BindingContext of the NoteEntryPage to the selected Note instance if the ListView has an item
            if (e.SelectedItem != null)
            {
                await Navigation.PushAsync(new NoteEntryPage
                {
                    BindingContext = e.SelectedItem as Note
                });
            }
        }
    }
}