﻿using System;
using System.IO;
using Xamarin.Forms;
using Notes.Models;

namespace Notes
{
    public partial class NoteEntryPage : ContentPage
    {
        public NoteEntryPage()
        {
            InitializeComponent();
        }

        //When the OnSaveButtonClicked event handler is executed
        async void OnSaveButtonClicked(object sender, EventArgs e)
        {
            //The Note instance is saved to the database and the application navigates back to the previous page
            var note = (Note)BindingContext;
            note.Date = DateTime.UtcNow;
            await App.Database.SaveNoteAsync(note);
            await Navigation.PopAsync();
        }

        //When the OnDeleteButtonClicked event handler is executed
        async void OnDeleteButtonClicked(object sender, EventArgs e)
        {
            //The Note instance is deleted from the database and the application navigates back to the previous page
            var note = (Note)BindingContext;
            await App.Database.DeleteNoteAsync(note);
            await Navigation.PopAsync();
        }
    }
}